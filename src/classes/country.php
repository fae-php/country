<?php
/**
 * FAE 
 */
namespace FAE\country;

use FAE\schema\model\model;

class country extends model {
  
  var $_model     = 'country';
  var $_modelFile = __DIR__ . '/../models/country.json';
  var $_defaultSort = ['name' => 'ASC'];
  
  static $initProgress = false;
  
  function getResultHook( $result, array $filter )
  {
    if( !self::$initProgress ){
	    self::$initProgress = true;
      if( $result->rowCount() == 0 && $this->count() == 0 ){
        self::$initProgress = true;
        self::initDb();
      }
    }
    return $result;
  }
  
  static function loadCountry($countryCode)
  {
    $country = new self();
    if(is_numeric($countryCode)){
      if( !$response = $country->get( ['id' => $countryCode] ) ){
        return false;
      }
    } else {
      if( !$response = $country->get( ['code' => $countryCode] ) ){
        return false;
      }
    }
    return $response->fetch();
  }
  
  static function initDb()
  {
    try {
      $dataPath = __DIR__ . '/../data/data.json';
      $dataRaw = file_get_contents($dataPath);
      $data = json_decode($dataRaw);
    } catch (\Exception $e) {
      echo 'Could not load country data file';
      return false;
    }
    //try {
      $instance = new self();
      foreach( $data as $country ){
        $instance->insert( (array) $country );
      }
    /*} catch (\Exception $e) {
      echo 'Could not insert country data file';
      return false;
    }*/
    return true;
  }
  
}